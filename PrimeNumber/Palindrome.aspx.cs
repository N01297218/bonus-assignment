﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrimeNumber
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {
            string input,reverse =  "";
            input = (userInput.Text).ToLower();
            input = input.Replace(" ","");

            for (int i = input.Length - 1; i >= 0; i--)
            {
                reverse += input[i].ToString();
            }
            if (reverse == input)
            {
                output.InnerHtml = "The string is palindrome";
            }
            else
            {
                output.InnerHtml = "The string is not palindrome";
            }

        }
    }
}