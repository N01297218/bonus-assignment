﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrol.aspx.cs" Inherits="PrimeNumber.servercontrol" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Check If Your Number Is Prime or Not</h2>
        </div>
        <label>Enter the number:</label><br />
        <asp:TextBox runat="server" ID="userInput"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator" ControlToValidate="userInput" Display="Static" ForeColor="DarkRed" ErrorMessage="Please enter a value" runat="server" />
            <asp:RegularExpressionValidator ID="Regularexp" ControlToValidate="userInput" runat="server" ErrorMessage="Only Numbers Please..!!" ValidationExpression="^[+-]?([0-9]*\.?[0-9]+[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">

            </asp:RegularExpressionValidator>
            
        <p><asp:Button ID="button" runat="server" OnClick="button_click" Text="Check" /></p>
        <div id="output" runat="server">

        </div>
    </form>
</body>
</html>
