﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="PrimeNumber.Palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <h2>Check If String is Palindrome or not</h2>
        </div>
        <label>Enter the string:</label><br />
        <asp:TextBox runat="server" ID="userInput"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator" ControlToValidate="userInput" Display="Static" ForeColor="DarkRed" ErrorMessage="Please enter valid string" runat="server" />
            
        <p><asp:Button ID="button" runat="server" OnClick="button_click" Text="Check" /></p>
        <div id="output" runat="server">

        </div>
    </form>
</body>
</html>
