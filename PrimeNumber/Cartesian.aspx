﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="PrimeNumber.Cartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Check Where x and y lies in the quadrant</h2>
        </div>
        <div>
            <label>Enter the value x:</label><br />
            <asp:TextBox runat="server" ID="userInput"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator" ControlToValidate="userInput" Display="Static" ForeColor="DarkRed" ErrorMessage="Please enter value for x" runat="server" />
            <asp:RegularExpressionValidator ID="Regularexp" ControlToValidate="userInput" runat="server" ErrorMessage="Only Numbers Please..!!" ValidationExpression="^[+-]?([0-9]*\.?[0-9]+[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">

            </asp:RegularExpressionValidator>
        </div>
        <div>
            <label>Enter the value y:</label><br />
            <asp:TextBox runat="server" ID="userInput2"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="userInput2" Display="Static" ForeColor="DarkRed" ErrorMessage="Please enter value for x" runat="server" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="userInput2" runat="server" ErrorMessage="Only Numbers Please..!!" ValidationExpression="^[+-]?([0-9]*\.?[0-9]+[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$">

            </asp:RegularExpressionValidator>
            <p>
                <asp:Button ID="button" runat="server" OnClick="button_click" Text="Check" />
            </p>
            <div id="output" runat="server">
            </div>
        </div>
    </form>
</body>
</html>
