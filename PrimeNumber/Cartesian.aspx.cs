﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrimeNumber
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {
            int num1, num2;
            num1= Convert.ToInt32(userInput.Text);
            num2= Convert.ToInt32(userInput2.Text);
            if(num1>0 && num2 >0)
            {
                output.InnerHtml = "(" + num1 + "," + num2 + ") lies in 1st quadrant";
            }
            else if(num1<0 && num2>0)
            {
                output.InnerHtml = "(" + num1 + "," + num2 + ") lies in 2nd quadrant";
            }
            else if(num1<0 && num2<0)
            {
                output.InnerHtml = "(" + num1 + "," + num2 + ") lies in 3rd quadrant";
            }
            else
            {
                output.InnerHtml = "(" + num1 + "," + num2 + ") lies in 4th quadrant";
            }
        }
    }
}