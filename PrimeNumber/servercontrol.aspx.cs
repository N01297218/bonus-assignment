﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrimeNumber
{
    public partial class servercontrol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void button_click(object sender, EventArgs e)
        {

            int num;
            num = Convert.ToInt32(userInput.Text);
            int flag = 0;

            for (int i = 2; i <= num / 2; i++)
            {
                if (num % i == 0)
                {
                    flag=1;
                    break;
                }
            }
            if (flag==0)
            {
                output.InnerHtml = "The integer is prime number";
            }
            else
            {
                output.InnerHtml = "The integer is not prime number";
            }
            
          
        }
    }
}